#!/usr/bin/env bash

# Stefan Behr
# LING 572
# Homework 8
# automation of all tests

# $1 is directory holding train2.txt and test2.txt
# $2 is model file path

files=$(echo train test)
ns=$(echo 1 5 10 20 50 100 150 200 250)

if [ ! -d sys ]
then
	echo Creating sys...
	mkdir sys
fi

if [ ! -d acc ]
then
	echo Creating acc...
	mkdir acc
fi

for file in $files
do
	for n in $ns
	do
		./TBL_classify.sh "$1/${file}2.txt" $2 "sys/${file}.out.${n}" $n >"acc/${file}.acc.${n}"
	done
done
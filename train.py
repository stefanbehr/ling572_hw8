#!/usr/bin/env python2.7

# Stefan Behr
# LING 572
# Homework 8
# training script

import argparse, utils

if __name__ == "__main__":
	parser = argparse.ArgumentParser("TBL training script")
	parser.add_argument("training_path", action="store", help="Path pointing to training data file")
	parser.add_argument("model_path", action="store", help="Path at which to store trained model")
	parser.add_argument("min_gain", action="store", type=int, help="Minimum gain below which to stop learning")

	args = parser.parse_args()

	# gold standard corpus
	training_corpus = utils.Corpus(args.training_path)
	# initially annotated corpus to be gradually transformed
	learning_corpus = training_corpus.initial_annotation(training_corpus.first_label())

	with open(args.model_path, "w") as model_file:
		print >> model_file, training_corpus.first_label()
		while True:
			gains = learning_corpus.net_gains(training_corpus)
			max_gain, best_rule = utils.dict_max_by_key(gains)
			if max_gain < args.min_gain:
				break
			learning_corpus.transform(best_rule)
			print >> model_file, "\t".join(best_rule + (str(max_gain),))
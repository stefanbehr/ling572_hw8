#!/usr/bin/env bash

# Stefan Behr
# LING 572
# Homework 8
# training script wrapper

# $1 is training data path
# $2 is model file output path
# $3 is minimum gain

python2.7 train.py $1 $2 $3
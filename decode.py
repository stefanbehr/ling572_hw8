#!/usr/bin/env python2.7

# Stefan Behr
# LING 572
# Homework 8
# decoding script

import argparse, utils

if __name__ == "__main__":
	parser = argparse.ArgumentParser("TBL decoding script")
	parser.add_argument("test_path", action="store", help="Path pointing to testing data file")
	parser.add_argument("model_path", action="store", help="Path pointing to TBL model")
	parser.add_argument("sys_path", action="store", help="Path at which to store system output")
	parser.add_argument("N", action="store", type=int, help="Maximum number of rules to apply from model file")
	args = parser.parse_args()

	with open(args.model_path) as model_file:
		default_label = model_file.readline().strip()
		
		gold_corpus = utils.Corpus(args.test_path)
		testing_corpus = gold_corpus.initial_annotation(default_label)

		# apply all transformations or first N, whichever is less
		for i, transformation in enumerate(model_file):
			if i >= args.N:
				break
			transformation = transformation.strip().split()
			if transformation:
				transformation = transformation[:-1]	# get rid of gain
				testing_corpus.transform(transformation)

		labels = list(set([label for label, features, history in gold_corpus.instances]))
		# initialize confusion matrix
		confusion = {true_label: {sys_label: 0 for sys_label in labels} for true_label in labels}
		correct = 0
		guesses = 0

		# check performance and output results
		with open(args.sys_path, "w") as sys_output:
			for i, instance in enumerate(testing_corpus.instances):
				sys_label, features, history = instance
				gold_label = gold_corpus.instances[i][0]
				# update confusion matrix
				confusion[gold_label][sys_label] += 1
				# update accuracy variables
				if sys_label == gold_label:
					correct += 1
				guesses += 1

				output = "array:{0} {1} {2}".format(i, gold_label, sys_label)
				history = " ".join(" ".join(transformation) for transformation in history)
				if history:
					output = "{0} {1}".format(output, history)

				print >> sys_output, output

		# calculate accuracy, output acc and confusion matrix
		acc = float(correct) / guesses

		print "Confusion matrix for the test data:"
		print "row is the truth, column is the system output"
		print
		print utils.pprint_matrix(confusion)
		print "\nTest accuracy={0}".format(acc)
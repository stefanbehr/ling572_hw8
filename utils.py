# Stefan Behr
# LING 572
# Homework 8

"""
This module contains utility functions and classes for 
LING 572, Homework 8, Transformation-Based Learning
"""

def isolate_feature(feature_value_string):
	"""
	Given a string of the form "<feature>:<value>", returns 
	"<feature>"
	"""
	return feature_value_string.split(":")[0]

def pprint_matrix(matrix):
	"""
	Given a matrix represented by nested dicts, 
	returns formatted string representation of matrix 
	for pretty-printing
	"""
	row_labels = sorted(matrix.keys())
	col_labels = sorted(matrix[row_labels[0]].keys())
	rows = ["\t".join([""] + col_labels)]
	for row_label in row_labels:
		row_dict = matrix[row_label]
		row = [row_label] + [str(matrix[row_label][col_label]) for col_label in col_labels]
		rows.append("\t".join(row))
	return "\n".join(rows)

def dict_max_by_key(d):
	"""
	Given a dict of key value pairs where 
	values are assumed to be numbers, return a 
	(value, key) tuple containing the key whose 
	value in the dict is highest. No guarantee is made 
	as to how ties are broken, due to unordered nature 
	of dicts.
	"""
	max_key, max_value = d.items()[0]
	for key, value in d.items():
		if value > max_value:
			max_value = value
			max_key = key
	return (max_value, max_key)

class Corpus:
	"""
	Corpus class for storing training and testing corpora 
	of document instances
	"""

	def __init__(self, corpus_path=None, instance_list=[]):
		if corpus_path is not None:
			self.instances = []
			with open(corpus_path) as corpus_file:
				for instance in corpus_file:
					instance = instance.strip().split()
					if instance:
						label = instance.pop(0)
						instance = map(isolate_feature, instance)
						self.instances.append([label, instance, []])
		else:
			self.instances = instance_list

	def labels(self):
		"""
		Return a list of all possible labels in corpus
		"""
		return list(set([label for label, features, history in self.instances]))

	def first_label(self):
		return self.instances[0][0]

	def initial_annotation(self, default_label):
		"""
		Given a corpus (self), returns a new corpus 
		whose instances are all labeled with a given 
		default label
		"""
		instances = [[default_label, features, transform_history] for label, features, transform_history in self.instances]
		return Corpus(instance_list=instances)

	def net_gains(self, truth):
		"""
		(this docstring is outdated; transformations are being generated 
		for increased efficiency)

		Given a corpus (self), a list of candidate transformations, 
		and a corpus representing the truth (correct classification of 
		each instance in the corpus), return a list of net gains that 
		would result in applying each transformation to the entire 
		corpus in turn.

		A transformation takes the form "<feature> <curLabel> <targetLabel>", 
		and applies to a document if the document contains <feature> and if the 
		document's current label is <curLabel>, changing the document's label 
		to <targetLabel>.

		A net gain is the amount of increase or reduction in error between 
		two corpora resulting from the application of a particular transformation 
		to one corpus.

		The amount of error between two corpora is equal to the number of label 
		mismatches between identical instances in the two corpora.
		"""

		labels = truth.labels()
		gains = {}

		for i, instance in enumerate(self.instances):
			gold_label = truth.instances[i][0]
			current_label, features, transform_history = instance
			for feature in features:
				# ignore null transformations this way
				target_labels = [label for label in labels if label != current_label]
				for target in target_labels:
					transformation = (feature, current_label, target)
					# this and current != target entails target != gold, net loss
					if current_label == gold_label:
						gains[transformation] = gains.get(transformation, 0) - 1
					# possible gain
					elif target == gold_label:
						gains[transformation] = gains.get(transformation, 0) + 1
		return gains

	def transform(self, transformation):
		"""
		Given a transformation of the form 
		(<feature>, <source_label>, <target_label>), 
		for all instances in this corpus, if the instance 
		contains <feature> and is labeled with <source_label>, 
		change the label of the instance to <target_label>
		"""
		feature, source, target = transformation
		for instance in self.instances:
			label, features, transform_history = instance
			if label == source and feature in features:
				# apply transform
				instance[0] = target
				instance[2].append(transformation)
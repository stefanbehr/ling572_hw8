#!/usr/bin/env bash

# Stefan Behr
# LING 572
# Homework 8
# testing script wrapper

# $1 is testing data path
# $2 is model file path
# $3 is system output file path
# $4 is limit on transformation applications

python2.7 decode.py $1 $2 $3 $4